package sw.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController { 

	@RequestMapping(value="/login.ya" ,method=RequestMethod.POST)
	public String postLogin(ModelMap map,HttpServletRequest request,HttpServletResponse response){
		
		String uname = request.getParameter("username");
		String pass = request.getParameter("password");
		
		if(uname.equals(pass))
			System.out.println(" Username and passord are same ");
		else
			System.out.println(" Username and passord are different ");
		
		return "first.ya";
	}
}
