package sw.controller;


import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sw.beans.Emp;

@Controller
public class SpringWebController { 
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	@RequestMapping(value="/first.ya" ,method=RequestMethod.POST)
	public String postFirstRequest(ModelMap map,HttpServletRequest request,HttpServletResponse response){
		System.out.println(" request received first.do.......");
		Emp emp =null;
		try {
			/*Session session = sessionFactory.openSession();
			session.beginTransaction();
			emp = new Emp();
			Random ran = new Random();
			emp.setEmpName("Ankit "+ran.nextInt());
			session.save(emp);
			 
			//Commit the transaction
			session.getTransaction().commit();
			
			// close session
			session.close();*/
			//System.out.println(" initialization complete........ Emp No. Created"+emp.getEmpName());
		} catch (HibernateException e) {
			System.out.println("exception.......");
			e.printStackTrace();
		}
		
		
		return "first.jsp";
	}
	
	@RequestMapping(value="/first.do" ,method=RequestMethod.GET)
	public String getFirstRequest(ModelMap map,HttpServletRequest request,HttpServletResponse response){
		System.out.println(" request received first.do.......");
		Emp emp =null;
		try {
			/*Session session = sessionFactory.openSession();
			session.beginTransaction();
			emp = new Emp();
			Random ran = new Random();
			emp.setEmpName("Ankit "+ran.nextInt());
			session.save(emp);
			 
			//Commit the transaction
			session.getTransaction().commit();
			
			// close session
			session.close();*/
			//System.out.println(" initialization complete........ Emp No. Created"+emp.getEmpName());
		} catch (HibernateException e) {
			System.out.println("exception.......");
			e.printStackTrace();
		}
		
		
		return "first.jsp";
	}
}
